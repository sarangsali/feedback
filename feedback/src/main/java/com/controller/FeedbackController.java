/**
 * 
 */
package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bean.feedback.FeedbackRate;
import com.bean.feedback.OwnerFeedback;
import com.service.FeedbackService;

/**
 * @author sarang_sali
 * 
 */

@RestController
@RequestMapping("/v1")
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;
 
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody List<OwnerFeedback> getFeedback() {
       
        return feedbackService.listFeedback();
    }
    
    @RequestMapping(value = "/rate", method = RequestMethod.GET)
    public @ResponseBody List<FeedbackRate> getFeedbackRates() {
       
        return feedbackService.listFeedbackRates();
    }
    
    @RequestMapping(value = "/", method = RequestMethod.POST,  headers = {"Accept=application/json"})
	public @ResponseBody OwnerFeedback addFeedback(@RequestBody OwnerFeedback ownerFeedback) {
		
		
		System.out.println("ownerFeedback : "+ownerFeedback);
		feedbackService.saveOrUpdateFeedback(ownerFeedback);

		return ownerFeedback;
	}
	
}
