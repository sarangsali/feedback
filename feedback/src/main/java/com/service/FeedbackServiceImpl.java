/**
 * 
 */
package com.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bean.feedback.FeedbackRate;
import com.bean.feedback.OwnerFeedback;
import com.dao.FeedbackDao;



/**
 * @author LENOVO
 *
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {
	
	 @Autowired
	 private FeedbackDao feedbackDao;

	/**
	 * 
	 */
	public FeedbackServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	@Transactional
	public List<OwnerFeedback> listFeedback() {
		      return feedbackDao.listFeedback();
	}
	
	@Override
	@Transactional
	public List<FeedbackRate> listFeedbackRates() {
		      return feedbackDao.listFeedbackRate();
	}

	@Override
	@Transactional
	public void saveOrUpdateFeedback(OwnerFeedback ownerFeedback) {
		
		ownerFeedback.setCreatedDate(new Date());
		ownerFeedback.setNextFeedbakDate(DateUtils.addMonths(new Date(), 3));
		ownerFeedback.getFeedbackRates().get(0).setOwnerFeedback(ownerFeedback);
		System.out.println("ownerFeedback : "+ownerFeedback);
		feedbackDao.saveOrUpdateFeedback(ownerFeedback);
		
	}

}
