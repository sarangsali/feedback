/**
 * 
 */
package com.service;

import java.util.List;

import com.bean.feedback.FeedbackRate;
import com.bean.feedback.OwnerFeedback;

/**
 * @author LENOVO
 *
 */
public interface FeedbackService {
	
	List<OwnerFeedback> listFeedback();
	
	public void saveOrUpdateFeedback(OwnerFeedback ownerFeedback);

	List<FeedbackRate> listFeedbackRates();

}
