/**
 * 
 */
package com.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bean.feedback.FeedbackRate;
import com.bean.feedback.OwnerFeedback;

/**
 * @author LENOVO
 *
 */
@Repository
public class FeedbackDaoImpl implements FeedbackDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private HibernateTemplate ht;

	/**
	 * 
	 */
	public FeedbackDaoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	@Transactional
	public List<OwnerFeedback> listFeedback() {
//		@SuppressWarnings("unchecked")
//	    TypedQuery<OwnerFeedback> query = sessionFactory.getCurrentSession().createQuery("from OwnerFeedback");
//	    return query.getResultList();
		
		List<OwnerFeedback> ownerFeedbackList=new ArrayList<OwnerFeedback>();
		try {
			ownerFeedbackList=ht.loadAll(OwnerFeedback.class);
		} catch (Exception e) {
			System.out.println("Unable to get ownerFeedbackList. " + e);
		}
		System.out.println("OwnerFeedback : "+ownerFeedbackList);
		return ownerFeedbackList;
	}
	
	@Override
	@Transactional
	public List<FeedbackRate> listFeedbackRate() {
		@SuppressWarnings("unchecked")
	    TypedQuery<FeedbackRate> query = sessionFactory.getCurrentSession().createQuery("from FeedbackRate");
	    return query.getResultList();
	}

	@Override
	public void saveOrUpdateFeedback(OwnerFeedback ownerFeedback) {
		
		//sessionFactory.getCurrentSession().save(ownerFeedback);
		
		System.out.println("ownerFeedback : " + ownerFeedback);
		try {
			ht.saveOrUpdate(ownerFeedback);
		} catch (Exception e) {
			System.out.println("Unable to save ownerFeedback. " + e);
		}
		
	}

	

}
