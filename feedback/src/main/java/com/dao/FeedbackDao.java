/**
 * 
 */
package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.bean.feedback.FeedbackRate;
import com.bean.feedback.OwnerFeedback;

/**
 * @author LENOVO
 *
 */
@Repository
public interface FeedbackDao {
	
	List<OwnerFeedback> listFeedback();
	
	public void saveOrUpdateFeedback(OwnerFeedback ownerFeedback);

	List<FeedbackRate> listFeedbackRate();

}
