/**
 * 
 */
package com.bean.feedback;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author LENOVO
 *
 */
@Entity
@Table(name = "feedbackRate")
public class FeedbackRate implements Serializable {
	
	@Id @GeneratedValue
	@Column(name = "ID")
	private UUID id;
	
	@JsonBackReference
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="Feedback_ID", nullable = false)
	private OwnerFeedback ownerFeedback;
	
	
	@Column(name = "Question_ID")
	private String feedbackQuestion;
	
	@Column(name = "Answer")
	private boolean answer;
	
	@Column(name = "Answer_Weightage")
	private int answerWeightage;
	
//	@Type(type="yes_no")
//	@Column(name = "Delete")
//	private boolean delete;

	/**
	 * 
	 */
	public FeedbackRate() {
		// TODO Auto-generated constructor stub
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	

	public OwnerFeedback getOwnerFeedback() {
		return ownerFeedback;
	}

	public void setOwnerFeedback(OwnerFeedback ownerFeedback) {
		this.ownerFeedback = ownerFeedback;
	}

	public String getFeedbackQuestion() {
		return feedbackQuestion;
	}

	public void setFeedbackQuestion(String feedbackQuestion) {
		this.feedbackQuestion = feedbackQuestion;
	}

	public boolean isAnswer() {
		return answer;
	}

	public void setAnswer(boolean answer) {
		this.answer = answer;
	}

	public int getAnswerWeightage() {
		return answerWeightage;
	}

	public void setAnswerWeightage(int answerWeightage) {
		this.answerWeightage = answerWeightage;
	}
	
//	public boolean isDelete() {
//		return delete;
//	}
//
//	public void setDelete(boolean delete) {
//		this.delete = delete;
//	}

	@Override
	public String toString() {
		return "FeedbackRate [id=" + id + ", feedbackQuestion=" + feedbackQuestion
				+ ", answer=" + answer + ", answerWeightage=" + answerWeightage + "]";
	}

	
	
	

}
