/**
 * 
 */
package com.bean.feedback;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author LENOVO
 *
 */
@Entity
@Table(name = "feedbackQuestion")
public class FeedbackQuestion implements Serializable {
	
	
	@Id @GeneratedValue
	@JoinColumn(name = "ID")
	private UUID id;
	
	@NotNull
	@Column(name = "Question_Name")
	private String questionName;
	
	@NotNull
	@Column(name = "Weightage")
	private int weightage;
	
	@NotNull
	@Column(name = "Question_Type")
	private String questionType;
	
	

	/**
	 * 
	 */
	public FeedbackQuestion() {
		// TODO Auto-generated constructor stub
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public int getWeightage() {
		return weightage;
	}

	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	

	@Override
	public String toString() {
		return "FeedbackQuestion [id=" + id + ", questionName=" + questionName + ", weightage=" + weightage
				+ ", questionType=" + questionType + "]";
	}
	
	

	
}
