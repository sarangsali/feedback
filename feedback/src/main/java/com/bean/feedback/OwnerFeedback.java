/**
 * 
 */
package com.bean.feedback;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author LENOVO
 *
 */
@Entity
@Table(name = "ownerFeedback", uniqueConstraints = { @UniqueConstraint(name = "UniqueIdAndDateKey", columnNames = { "Prop_ID", "From_ID", "Owner_ID" }) })
public class OwnerFeedback implements Serializable {
	
	@Id @GeneratedValue
	@Column(name = "ID")
	private UUID id;
	
	@NotNull
	@Column(name = "Prop_ID")
	private String propId;
	
	@NotNull
	@Column(name = "From_ID")
	private String fromId;
	
	@NotNull
	@Column(name = "Owner_ID")
	private String ownerId;
	
	@NotNull
	@Column(name = "Created_Date")
	private Date createdDate;
	
	@NotNull
	@Column(name = "Next_Feedback_Date")
	private Date nextFeedbakDate;
	
	
	@Column(name = "Updated_Date")
	private Date updatedDate;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "ownerFeedback", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<FeedbackRate> feedbackRates= new ArrayList<>();
	
	
//	@Type(type="yes_no")
//	@Column(name = "Delete")
//	private boolean delete;

	/**
	 * 
	 */
	public OwnerFeedback() {
		// TODO Auto-generated constructor stub
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getPropId() {
		return propId;
	}

	public void setPropId(String propId) {
		this.propId = propId;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(String fromId) {
		this.fromId = fromId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getNextFeedbakDate() {
		return nextFeedbakDate;
	}

	public void setNextFeedbakDate(Date nextFeedbakDate) {
		this.nextFeedbakDate = nextFeedbakDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	

//	public boolean isDelete() {
//		return delete;
//	}
//
//	public void setDelete(boolean delete) {
//		this.delete = delete;
//	}
	
	

	public List<FeedbackRate> getFeedbackRates() {
		return feedbackRates;
	}

	public void setFeedbackRates(List<FeedbackRate> feedbackRates) {
		this.feedbackRates = feedbackRates;
	}

	@Override
	public String toString() {
		return "OwnerFeedback [id=" + id + ", propId=" + propId + ", fromId=" + fromId + ", ownerId=" + ownerId
				+ ", createdDate=" + createdDate + ", nextFeedbakDate=" + nextFeedbakDate + ", updatedDate="
				+ updatedDate + "]";
	}



	

	
	

}
