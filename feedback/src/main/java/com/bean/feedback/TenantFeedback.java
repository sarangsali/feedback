/**
 * 
 */
package com.bean.feedback;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

/**
 * @author LENOVO
 *
 */
@Entity
@Table(name = "tenantFeedback", uniqueConstraints = { @UniqueConstraint(name = "UniqueIdAndDateKey", columnNames = { "Prop_ID", "From_ID", "Tenant_ID", "Created_Date" }) })
public class TenantFeedback implements Serializable {
	
	@Id @GeneratedValue
	@Column(name = "ID")
	private UUID id;
	
	@NotNull
	@Column(name = "Prop_ID")
	private String propId;
	
	@NotNull
	@Column(name = "From_ID")
	private String fromId;
	
	@NotNull
	@Column(name = "Tenant_ID")
	private String tenantId;
	
	@NotNull
	@Column(name = "Created_Date")
	private Date createdDate;
	
	@NotNull
	@Column(name = "Next_Feedback_Date")
	private Date nextFeedbakDate;
	
	@Column(name = "Updated_Date")
	private Date updatedDate;
	
	@Column(name = "Feedback_Rate")
	private int feedbackRate;
	
//	@Type(type="yes_no")
//	@Column(name = "Delete")
//	private boolean delete;

	/**
	 * 
	 */
	public TenantFeedback() {
		// TODO Auto-generated constructor stub
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getPropId() {
		return propId;
	}

	public void setPropId(String propId) {
		this.propId = propId;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(String fromId) {
		this.fromId = fromId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getNextFeedbakDate() {
		return nextFeedbakDate;
	}

	public void setNextFeedbakDate(Date nextFeedbakDate) {
		this.nextFeedbakDate = nextFeedbakDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getFeedbackRate() {
		return feedbackRate;
	}

	public void setFeedbackRate(int feedbackRate) {
		this.feedbackRate = feedbackRate;
	}
	
//	public boolean isDelete() {
//		return delete;
//	}
//
//	public void setDelete(boolean delete) {
//		this.delete = delete;
//	}

	@Override
	public String toString() {
		return "TenantFeedback [id=" + id + ", propId=" + propId + ", fromId=" + fromId + ", tenantId=" + tenantId
				+ ", createdDate=" + createdDate + ", nextFeedbakDate=" + nextFeedbakDate + ", updatedDate="
				+ updatedDate + ", feedbackRate=" + feedbackRate + "]";
	}

	
	
	

}
