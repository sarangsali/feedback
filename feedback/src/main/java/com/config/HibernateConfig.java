/**
 * 
 */
package com.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author LENOVO
 *
 */
@Configuration
@EnableTransactionManagement
public class HibernateConfig {

	/**
	 * 
	 */
	public HibernateConfig() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
    private ApplicationContext context;
 
    @Bean
    public LocalSessionFactoryBean getSessionFactory() {
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setConfigLocation(context.getResource("classpath:hibernate.cfg.xml"));
        factoryBean.setPackagesToScan("com.bean.feedback");
        return factoryBean;
    }
 
    @Bean
    public HibernateTransactionManager getTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(getSessionFactory().getObject());
        return transactionManager;
    }
    
    @Bean
    public HibernateTemplate getHibernateTemplate() {
    	HibernateTemplate hibernateTemplate = new HibernateTemplate();
    	hibernateTemplate.setSessionFactory(getSessionFactory().getObject());
        return hibernateTemplate;
    }
    
 

}
